<!DOCTYPE html>
<html>
 <head>
  <title>Product add</title>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" />
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

 </head>
 <body>
  <br /><br />
  <div class="container">
   <br />
   <h2 align="center">Product Add</h2>
   <br />
   <div class="table-responsive">
    <table class="table table-bordered" id="crud_table">
     <tr>
      <th width="30%">SKU</th>
      <th width="10%">Name</th>
      <th width="45%">Description</th>
      <th width="10%">Price</th>
     </tr>
     <tr>
      <td contenteditable="true" class="SKU"></td>
      <td contenteditable="true" class="Name"></td>
      <td contenteditable="true" class="Description"></td>
      <td contenteditable="true" class="Price"></td>
     </tr>
    </table>
    <div align="center">
     <button type="button" name="save" id="save" class="btn btn-info">Save</button>
    </div>
    <br />
    <div id="inserted_item_data"></div>
   </div>

  </div>
 </body>
</html>

<script>
$(document).ready(function(){
 var count = 1;
 $('#add').click(function(){
  count = count + 1;
  var html_code = "<tr id='row"+count+"'>";
   html_code += "<td contenteditable='true' class='SKU'></td>";
   html_code += "<td contenteditable='true' class='Name'></td>";
   html_code += "<td contenteditable='true' class='Description'></td>";
   html_code += "<td contenteditable='true' class='Price' ></td>";
   html_code += "</tr>";
   $('#crud_table').append(html_code);
 });


 $('#save').click(function(){
  var SKU = [];
  var Name = [];
  var Description = [];
  var Price = [];
  $('.SKU').each(function(){
   SKU.push($(this).text());
  });
  $('.Name').each(function(){
   Name.push($(this).text());
  });
  $('.Description').each(function(){
   Description.push($(this).text());
  });
  $('.Price').each(function(){
   Price.push($(this).text());
  });
  $.ajax({
   url:"insert.php",
   method:"POST",
   data:{SKU:SKU, Name:Name, Description:Description, Price:Price},
   success:function(data){
    alert(data);
    $("td[contentEditable='true']").text("");
    for(var i=2; i<= count; i++)
    {
     $('tr#'+i+'').remove();
    }
    fetch_item_data();
   }
  });
 });

 function fetch_item_data()
 {
  $.ajax({
   url:"fetch.php",
   method:"POST",
   success:function(data)
   {
    $('#inserted_item_data').html(data);
   }
  })
 }
 fetch_item_data();

});
</script>
