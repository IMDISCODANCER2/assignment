
<?php
//fetch.php
$connect = mysqli_connect("localhost:3306", "root2", "", "testing");
$output = '';
$query = "SELECT * FROM assignment";
$result = mysqli_query($connect, $query);
$output = '
<br />
<h3 align="center">Added Items Data</h3>
<table class="table table-bordered table-striped">
 <tr>
  <th width="30%">SKU</th>
  <th width="10%">Name</th>
  <th width="50%">Description</th>
  <th width="10%">Price</th>
 </tr>
';
while($row = mysqli_fetch_array($result))
{
 $output .= '
 <tr>
  <td>'.$row["SKU"].'</td>
  <td>'.$row["Name"].'</td>
  <td>'.$row["Price"].'</td>
  <td>'.$row["Description"].'</td>
 </tr>
 ';
}
$output .= '</table>';
echo $output;
?>
