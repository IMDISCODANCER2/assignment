<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <title>Product List</title>
    <link rel="stylesheet" href="css/style.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
    <link href="https://fonts.googleapis.com/css?family=Maven+Pro&display=swap" rel="stylesheet">
  </head>
  <body>

   <div class="header">
      <p id="PLtop">Product list</p>
    <!--  <div class="dropdown">
        <button class="dropbtn">Mass Delete Action</button>
        <div class="dropdown-content">
          <a href="#">Option 1</a>
          <a href="#">Option 2</a>
          <a href="#">Option 3</a>

        </div>
      </div>
      <button class="buttonHeader">Apply</button>  -->
    </div>


        <main>
      <div class="goods-out">

      </div>
    </main>

  </body>
</html>

<script>
function displayItems()
{
 $.ajax({
  url:"display.php",
  method:"POST",
  success:function(data)
  {
   $('.goods-out').html(data);
  }
 })
}
displayItems();
</script>
